#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Lex/Lexer.h"

using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;

class Record 
{
public:
    Record(VarDecl d): decl(d) {}

    VarDecl decl;
    std::vector<SourceLocation> refer;
};

class ListVar {
private:
    std::vector<Record> list; // List of variables and their referred location
    SourceManager *SM; // For printing SourceLocation

public:
    // Add a referred location for a variable
    void add(ValueDecl *var, SourceLocation src) {
        for(std::vector<Record>::iterator it = list.begin();
                it != list.end(); ++it) {
            if(it->decl.getLocation().getRawEncoding() == 
                    var->getLocation().getRawEncoding()) {
                it->refer.push_back(src);
                return;
            }
        }
    }

    // Add a new variable
    void add(VarDecl *var) {
        Record rec(*var);
        list.push_back(rec);
    }

    // Print out all variables and their referred location
    void print() {
        for(std::vector<Record>::iterator it = list.begin();
                it != list.end(); ++it) {
            llvm::errs() << it->decl.getNameAsString() << " (" 
                << it->decl.getType().getAsString () << ") (";
            it->decl.getLocation().dump(*SM);
            llvm::errs() << ")\n";
            for(std::vector<SourceLocation>::iterator src = it->refer.begin(); 
                    src != it->refer.end(); src++) {
                llvm::errs() << "|--";
                src->dump(*SM);
                llvm::errs() << "\n";
            }
            llvm::errs() << "\n";
        }
    }

    // Set SourceManager used for printing SourceLocation
    void setSM(SourceManager *sm) {
        SM = sm;
    }
};

ListVar var;

class ListVarVisitor : public RecursiveASTVisitor<ListVarVisitor> {
private:
    ASTContext *astContext; // used for getting additional AST info
    SourceManager *SM;

public:
    explicit ListVarVisitor(CompilerInstance *CI) 
      : astContext(&(CI->getASTContext())), 
        SM(&(astContext->getSourceManager()))
    {
        var.setSM(SM);
    }

    virtual bool VisitStmt(Stmt *st) {
        // Ignore if the statement is in System Header files
        if(!st->getLocStart().isValid() || 
                SM->isInSystemHeader(st->getLocStart()))
            return true;

        if(DeclRefExpr *d = dyn_cast<DeclRefExpr>(st)) {
            var.add(d->getDecl(), getCodeLoc(d->getLocation()));
        } 
        return true;
    }

    virtual bool VisitDecl(Decl *decl) {
        // Ignore if the declaration is in System Header files
        if(!decl->getLocation().isValid() || 
                SM->isInSystemHeader(decl->getLocation()))
            return true;

        if(VarDecl *v = dyn_cast<VarDecl>(decl)) {
            var.add(v);
        }

        return RecursiveASTVisitor::VisitDecl(decl);
    }

private:
    // Function to get the location of the expanded code
    // If the code is a macro it'll expand the code and then return location
    SourceLocation getCodeLoc(SourceLocation src) {
        if(src.isFileID())
            return src;
        return SM->getExpansionLoc(src);
    }
};

class ListVarASTConsumer : public ASTConsumer {
private:
    ListVarVisitor *visitor;

public:
    explicit ListVarASTConsumer(CompilerInstance *CI)
        : visitor(new ListVarVisitor(CI)) // initialize the visitor
    { }

    virtual void HandleTranslationUnit(ASTContext &Context) {
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
        var.print();
    }
};

class ListVarFrontendAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, 
            StringRef file) {
        return std::unique_ptr<ASTConsumer>(new ListVarASTConsumer(&CI)); 
    }
};

int main(int argc, const char **argv) {
    llvm::cl::OptionCategory ClangCheckCategory("clang-list-var options");
    // parse the command-line args passed to your code
    CommonOptionsParser op(argc, argv, ClangCheckCategory);

    // create a new Clang Tool instance (a LibTooling environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());

    // run the Clang Tool, creating a new FrontendAction
    return Tool.run(newFrontendActionFactory<ListVarFrontendAction>().get());
}
